#!/bin/sh

# This script expediates the build process for Weeb++.

# It accepts a single command line argument indicating whether or not the
# directory should be cleaned before building.

# It does not standardize the working directory, so it must be run from the root
# of the project.

CLEAN=false

for option in "$@" 
do
    case "${option}" in
        -c|--clean) CLEAN=true;;
        *) echo "WARN: Unrecognized option passed to build.sh; ignoring...";;
    esac
done

if [[ "$CLEAN" == "true" ]]; then
    rm -rf build/ &> /dev/null
    rm -rf weeb++ &> /dev/null
fi

if ! cmake --version; then
    echo "ERROR: CMake must be installed in order to build this project."
    exit 1
fi

cmake -S . -B build && cmake --build build && cp build/src/weeb++ ./

if [[ "$?" != "0" ]]; then
    echo "ERROR: Failed to build the Weeb++ project."
    exit 1
fi

echo "INFO: Successfully built the Weeb++ project."