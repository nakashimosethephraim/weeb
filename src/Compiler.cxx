/*
 * File:   Compiler.cxx
 * Author: Seth Nakashimo
 * Date:   2023/02/06
 */

#include <locale>
#include <iostream>
#include <string>

#include "ArgParse.hxx"
#include "Lexer.hxx"
#include "WStringManip.hxx"

static void init_locale()
{
  constexpr char locale_name[] = "ja_JP.UTF-8";
  setlocale(LC_ALL, locale_name);
  std::locale::global(std::locale(locale_name));
  std::wcout.imbue(std::locale());
}

int main(int arc, char* argv[])
{
  init_locale();

  const std::wstring prompt = L"＞　";
  std::wcout << prompt;

  std::string line;
  std::wstring converted;
  while (getline(std::cin, line))
  {
    converted = WStringManip::convert(line);
    WStringManip::trim(converted);

    Lexer lexer(converted);
    auto token = lexer.next_token();
    while (token->value().get_interface() != SyntaxToken::SyntaxTokenDataType::Newline)
    {
      std::wcout << *token << std::endl << std::endl;
      token = lexer.next_token();
    }

    if (converted.empty()) break;

    std::wcout << std::endl << prompt;
  }

  converted = L"";
  Lexer lexer(converted);
  auto token = lexer.next_token();
  std::wcout << *token << std::endl;

  return 0;
}