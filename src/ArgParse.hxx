/* 
 * File:   "ArgParse.hxx"
 * Author: Seth Nakashimo
 * Date:   2022/01/26
 *
 * A header file defining a simple class to facilitate pulling options and arguments
 * from the command line.
 */

#ifndef ARGPARSE_HXX
#define ARGPARSE_HXX

#include <string_view>
#include <vector>

class ArgParse
{
private:
  std::vector<std::string_view> args;

public:
  ArgParse(char* argv_begin[], char* argv_end[]): args(argv_begin, argv_end) {}

  /*
   * This function checks if the contents of the given string view are
   * present in the found command line arguments.
   *
   * @param option A named option
   * @return Whether or not the given option was present
   */  
  bool has_option(const std::string_view& option) const;

  /*
   * This function returns the value associated with the given
   * option as a string view if found, or an empty string view
   * otherwise.
   *
   * @param option A named option
   * @return The value associated with the given option as a string view if
   *         found, or an empty string view otherwise
   */  
  std::string_view get_option(const std::string_view& option) const;
};

#endif /* ARGPARSE_HXX */
