/*
 * File:   Syntax.hxx
 * Author: Seth Nakashimo
 * Date:   2023/02/03
 */

#ifndef SYNTAX_HXX
#define SYNTAX_HXX

#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>

enum class ReservedToken : std::uint8_t
{
  Unknown,

  OpParenthesisOpen,
  OpParenthesisClose,
  OpSubscriptOpen,
  OpSubscriptClose,

  OpArrow,
  OpColon,
  OpComma,
  OpDot,

  // Arithmetic operators
  OpAdd,
  OpDiv,
  OpExp,
  OpFloorDiv,
  OpMod,
  OpMul,
  OpSub,

  // Bitwise operators
  OpBitwiseAnd,
  OpBitwiseLeftShift,
  OpBitwiseNot,
  OpBitwiseOr,
  OpBitwiseRightShift,
  OpBitwiseXor,

  // Logical operators
  OpLogicalAnd,
  OpLogicalNot,
  OpLogicalOr,

  // Assignment operators
  OpAssign,
  OpAssignAdd,
  OpAssignDiv,
  OpAssignExp,
  OpAssignFloorDiv,
  OpAssignMod,
  OpAssignMul,
  OpAssignSub,

  // Bitwise assignment operators
  OpAssignBitwiseAnd,
  OpAssignBitwiseLeftShift,
  OpAssignBitwiseOr,
  OpAssignBitwiseRightShift,
  OpAssignBitwiseXor,

  // Comparison operators
  OpEqual,
  OpGreaterThan,
  OpGreaterThanEqual,
  OpLessThan,
  OpLessThanEqual,
  OpNotEqual,

  // Keywords
  KwAnd,
  KwAs,
  KwAssert,
  KwBreak,
  KwCalled,
  KwClass,
  KwContinue,
  KwContract,
  KwDef,
  KwDel,
  KwElse,
  KwExcept,
  KwFalse,
  KwFinally,
  KwFor,
  KwFrom,
  KwGlobal,
  KwIf,
  KwImport,
  KwIn,
  KwIs,
  KwLambda,
  KwNone,
  KwNot,
  KwNumber,
  KwOr,
  KwPass,
  KwRaise,
  KwReturn,
  KwSetTo,
  KwString,
  KwTrue,
  KwTry,
  KwWhile,
  KwWith,
  KwYield,

  // Number of possible reserved tokens
  Count
};

struct StringifiedReservedToken
{
  std::wstring_view token_str;
  ReservedToken token{ ReservedToken::Unknown };

  [[nodiscard]]
  constexpr bool operator<(StringifiedReservedToken const& rhs) const
  {
    return token_str < rhs.token_str;
  }

  friend std::wostream& operator<<(std::wostream& stream, const StringifiedReservedToken& token);
};

namespace SyntaxConstants
{
  template<typename T, std::size_t N>
  [[nodiscard]]
  constexpr std::array<T, N> sort(std::array<T, N> arr) noexcept
  {
    std::sort(std::begin(arr), std::end(arr));
    return arr;
  }

  constexpr auto keywords
  {
    sort
    (
      std::array
      {
        StringifiedReservedToken{ L"偽り" , ReservedToken::KwFalse },
        StringifiedReservedToken{ L"無し" , ReservedToken::KwNone },
        StringifiedReservedToken{ L"真実" , ReservedToken::KwTrue },
        StringifiedReservedToken{ L"と" , ReservedToken::KwAnd },
        StringifiedReservedToken{ L"と呼ばれて" , ReservedToken::KwAs },
        StringifiedReservedToken{ L"主張して" , ReservedToken::KwAssert },
        StringifiedReservedToken{ L"脱出して" , ReservedToken::KwBreak },
        StringifiedReservedToken{ L"という" , ReservedToken::KwCalled },
        StringifiedReservedToken{ L"分類" , ReservedToken::KwClass },
        StringifiedReservedToken{ L"続けて" , ReservedToken::KwContinue },
        StringifiedReservedToken{ L"契約" , ReservedToken::KwContract },
        StringifiedReservedToken{ L"定義" , ReservedToken::KwDef },
        StringifiedReservedToken{ L"消除して" , ReservedToken::KwDel },
        StringifiedReservedToken{ L"そうじゃなかったら" , ReservedToken::KwElse },
        StringifiedReservedToken{ L"が起こった場合は" , ReservedToken::KwExcept },
        StringifiedReservedToken{ L"最後に" , ReservedToken::KwFinally },
        StringifiedReservedToken{ L"ごとに" , ReservedToken::KwFor },
        StringifiedReservedToken{ L"から" , ReservedToken::KwFrom },
        StringifiedReservedToken{ L"どこでも使える" , ReservedToken::KwGlobal },
        StringifiedReservedToken{ L"場合は" , ReservedToken::KwIf },
        StringifiedReservedToken{ L"輸入して" , ReservedToken::KwImport },
        StringifiedReservedToken{ L"に入っているもの" , ReservedToken::KwIn },
        StringifiedReservedToken{ L"は" , ReservedToken::KwIs },
        StringifiedReservedToken{ L"適当に" , ReservedToken::KwLambda },
        StringifiedReservedToken{ L"じゃない" , ReservedToken::KwNot },
        StringifiedReservedToken{ L"数" , ReservedToken::KwNumber },
        StringifiedReservedToken{ L"か" , ReservedToken::KwOr },
        StringifiedReservedToken{ L"無効" , ReservedToken::KwPass },
        StringifiedReservedToken{ L"挙げて" , ReservedToken::KwRaise },
        StringifiedReservedToken{ L"返して" , ReservedToken::KwReturn },
        StringifiedReservedToken{ L"に設定して" , ReservedToken::KwSetTo },
        StringifiedReservedToken{ L"文字列" , ReservedToken::KwString },
        StringifiedReservedToken{ L"これ試して" , ReservedToken::KwTry },
        StringifiedReservedToken{ L"間は" , ReservedToken::KwWhile },
        StringifiedReservedToken{ L"が" , ReservedToken::KwWith },
        StringifiedReservedToken{ L"出して" , ReservedToken::KwYield }
      }
    )
  };

  constexpr auto operators
  {
    sort
    (
      std::array
      {
        StringifiedReservedToken{ L"！" , ReservedToken::OpLogicalNot },
        StringifiedReservedToken{ L"！＝" , ReservedToken::OpNotEqual },
        StringifiedReservedToken{ L"％" , ReservedToken::OpMod },
        StringifiedReservedToken{ L"を％" , ReservedToken::OpAssignMod },
        StringifiedReservedToken{ L"＆" , ReservedToken::OpBitwiseAnd },
        StringifiedReservedToken{ L"＆＆" , ReservedToken::OpLogicalAnd },
        StringifiedReservedToken{ L"を＆" , ReservedToken::OpAssignBitwiseAnd },
        StringifiedReservedToken{ L"（" , ReservedToken::OpParenthesisOpen },
        StringifiedReservedToken{ L"）" , ReservedToken::OpParenthesisClose },
        StringifiedReservedToken{ L"＊" , ReservedToken::OpMul },
        StringifiedReservedToken{ L"＊＊" , ReservedToken::OpExp },
        StringifiedReservedToken{ L"を＊＊" , ReservedToken::OpAssignExp },
        StringifiedReservedToken{ L"を＊" , ReservedToken::OpAssignMul },
        StringifiedReservedToken{ L"＋" , ReservedToken::OpAdd },
        StringifiedReservedToken{ L"を＋" , ReservedToken::OpAssignAdd },
        StringifiedReservedToken{ L"、" , ReservedToken::OpComma },
        StringifiedReservedToken{ L"ー" , ReservedToken::OpSub },
        StringifiedReservedToken{ L"ー＝" , ReservedToken::OpAssignSub },
        StringifiedReservedToken{ L"→" , ReservedToken::OpArrow },
        StringifiedReservedToken{ L"．" , ReservedToken::OpDot },
        StringifiedReservedToken{ L"／" , ReservedToken::OpDiv },
        StringifiedReservedToken{ L"／／" , ReservedToken::OpFloorDiv },
        StringifiedReservedToken{ L"を／／" , ReservedToken::OpAssignFloorDiv },
        StringifiedReservedToken{ L"を／" , ReservedToken::OpAssignDiv },
        StringifiedReservedToken{ L"：" , ReservedToken::OpColon },
        StringifiedReservedToken{ L"＜" , ReservedToken::OpLessThan },
        StringifiedReservedToken{ L"＜＜" , ReservedToken::OpBitwiseLeftShift },
        StringifiedReservedToken{ L"を＜＜" , ReservedToken::OpAssignBitwiseLeftShift },
        StringifiedReservedToken{ L"＜＝" , ReservedToken::OpLessThanEqual },
        StringifiedReservedToken{ L"を" , ReservedToken::OpAssign },
        StringifiedReservedToken{ L"＝" , ReservedToken::OpEqual },
        StringifiedReservedToken{ L"＞" , ReservedToken::OpGreaterThan },
        StringifiedReservedToken{ L"＞＝" , ReservedToken::OpGreaterThanEqual },
        StringifiedReservedToken{ L"＞＞" , ReservedToken::OpBitwiseRightShift },
        StringifiedReservedToken{ L"を＞＞" , ReservedToken::OpAssignBitwiseRightShift },
        StringifiedReservedToken{ L"【" , ReservedToken::OpSubscriptOpen },
        StringifiedReservedToken{ L"】" , ReservedToken::OpSubscriptClose },
        StringifiedReservedToken{ L"＾" , ReservedToken::OpBitwiseXor },
        StringifiedReservedToken{ L"を＾" , ReservedToken::OpAssignBitwiseXor },
        StringifiedReservedToken{ L"｜" , ReservedToken::OpBitwiseOr },
        StringifiedReservedToken{ L"を｜" , ReservedToken::OpAssignBitwiseOr },
        StringifiedReservedToken{ L"｜｜" , ReservedToken::OpLogicalOr },
        StringifiedReservedToken{ L"〜" , ReservedToken::OpBitwiseNot }
      }
    )
  };

  constexpr auto valid_operators
  {
    sort
    (
      std::array
      {
        L'！',
        L'＝',
        L'％',
        L'を',
        L'＆',
        L'（',
        L'）',
        L'＊',
        L'＋',
        L'、',
        L'ー',
        L'→',
        L'．',
        L'／',
        L'：',
        L'＜',
        L'＞',
        L'【',
        L'】',
        L'＾',
        L'｜',
        L'〜'
      }
    )
  };
}

namespace Syntax
{
  [[nodiscard]] bool is_string_begin(const wchar_t c) noexcept;
  [[nodiscard]] bool is_string_end(const wchar_t c) noexcept;
  [[nodiscard]] bool is_valid_operator(const wchar_t c) noexcept;
  [[nodiscard]] bool is_digit(const wchar_t c) noexcept;
  [[nodiscard]] bool is_valid_identifier(const wchar_t c) noexcept;
  [[nodiscard]] bool is_whitespace(const wchar_t c) noexcept;

  [[nodiscard]] bool is_keyword(const std::wstring_view key) noexcept;
  [[nodiscard]] bool is_operator(const std::wstring_view key) noexcept;

  [[nodiscard]] const StringifiedReservedToken& get_keyword(const std::wstring_view key);
  [[nodiscard]] const StringifiedReservedToken& get_operator(const std::wstring_view key);
}

#endif /* SYNTAX_HXX */