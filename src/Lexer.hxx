/*
 * File:   Lexer.hxx
 * Author: Seth Nakashimo
 * Date:   2023/02/06
 */

#ifndef LEXER_HXX
#define LEXER_HXX

#include <memory>
#include <string>

#include "Syntax.hxx"
#include "SyntaxToken.hxx"

class Lexer
{
private:
  const std::wstring& text;
  size_t position;

  const wchar_t current() const;
  void next() { ++position; }

public:
  Lexer(const std::wstring& text): text(text), position(0) {}

  std::unique_ptr<SyntaxToken> next_token();
};

#endif /* LEXER_HXX */