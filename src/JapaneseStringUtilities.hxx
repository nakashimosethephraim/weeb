/* 
 * File:   "JapaneseStringUtilities.hxx"
 * Author: Seth Nakashimo
 * Date:   2022/02/06
 */

#ifndef JAPANESESTRINGUTILITIES_HXX
#define JAPANESESTRINGUTILITIES_HXX

#include <string>

namespace JapaneseStringUtilities
{
  const std::uint32_t wston(const std::wstring_view num_string);
}

#endif /* JAPANESESTRINGUTILITIES_HXX */