/* 
 * File:   "WStringManip.cxx"
 * Author: Seth Nakashimo
 * Date:   2022/02/06
 *
 * A file defining functionality useful for manipulating
 * std::wstring.
 */

#include "WStringManip.hxx"

namespace WStringManip
{
  inline void ltrim(std::wstring& s)
  {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](wchar_t ch) {
        return !std::isspace(ch);
    }));
  }

  inline void rtrim(std::wstring& s)
  {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](wchar_t ch) {
        return !std::isspace(ch);
    }).base(), s.end());
  }
}

void WStringManip::trim(std::wstring& s)
{
  rtrim(s);
  ltrim(s);
}

std::wstring WStringManip::convert(const std::string& source)
{
  std::wstring ws(source.size(), L' ');
  ws.resize(std::mbstowcs(&ws[0], source.c_str(), source.size()));
  return ws;
}