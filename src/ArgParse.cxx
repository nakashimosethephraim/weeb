/* 
 * File:   "ArgParse.cxx"
 * Author: Seth Nakashimo
 * Date:   2022/01/26
 *
 * A file defining a simple class to facilitate pulling options and arguments
 * from the command line.
 */

#include "ArgParse.hxx"

bool ArgParse::has_option(const std::string_view& option) const
{
    for (auto iter = args.begin(); iter != args.end(); ++iter)
    {
        if (*iter == option) return true;
    }
    
    return false;
}

std::string_view ArgParse::get_option(const std::string_view& option) const
{
    for (auto iter = args.begin(), end = args.end(); iter != end; ++iter)
    {
        if (*iter == option)
        {
            if (iter + 1 != end) return *(iter + 1);
        }
    }
    
    return "";
}
