/*
 * File:   Syntax.cxx
 * Author: Seth Nakashimo
 * Date:   2023/02/03
 */

#include "Syntax.hxx"

std::wostream& operator<<(std::wostream& stream, const StringifiedReservedToken& token)
{
  stream << L"Reserved Token: " << token.token_str;
  return stream;
}

namespace Syntax
{
  namespace
  {
    [[nodiscard]] std::unordered_set<wchar_t> initialize_valid_operator_set() noexcept
    {
      std::unordered_set<wchar_t> new_valid_operator_map;
      for (const auto& valid_operator : SyntaxConstants::valid_operators) new_valid_operator_map.insert(valid_operator);
      return new_valid_operator_map;
    }

    [[nodiscard]] std::unordered_map<std::wstring_view,size_t> initialize_keyword_map() noexcept
    {
      std::unordered_map<std::wstring_view,size_t> new_keyword_map;
      for (size_t i = 0; i < SyntaxConstants::keywords.size(); ++i) new_keyword_map[SyntaxConstants::keywords[i].token_str] = i;
      return new_keyword_map;
    }

    [[nodiscard]] std::unordered_map<std::wstring_view,size_t> initialize_operator_map() noexcept
    {
      std::unordered_map<std::wstring_view,size_t> new_operator_map;
      for (size_t i = 0; i < SyntaxConstants::operators.size(); ++i) new_operator_map[SyntaxConstants::operators[i].token_str] = i;
      return new_operator_map;
    }

    const std::unordered_set<wchar_t> valid_operator_set = initialize_valid_operator_set();
    const std::unordered_map<std::wstring_view,size_t> keyword_map = initialize_keyword_map();
    const std::unordered_map<std::wstring_view,size_t> operator_map = initialize_operator_map();
  }
}

bool Syntax::is_string_begin(const wchar_t c) noexcept
{
  return c == L'「';
}

bool Syntax::is_string_end(const wchar_t c) noexcept
{
  return c == L'」';
}

bool Syntax::is_valid_operator(const wchar_t c) noexcept
{
  return valid_operator_set.find(c) != valid_operator_set.end(); 
}

bool Syntax::is_digit(const wchar_t c) noexcept
{
  return c >= L'０' && c <= L'９';
}

bool Syntax::is_valid_identifier(const wchar_t c) noexcept
{
  return c != L'を' &&
         ((c >= L'ぁ' && c <= L'ゖ') ||
          (c >= L'ゝ' && c <= L'ゟ') ||
          (c >= L'ァ' && c <= L'ヿ') ||
          (c >= L'Ａ' && c <= L'Ｚ') ||
          c == L'＿' ||
          (c >= L'ａ' && c <= L'ｚ') ||
          (c >= L'ｦ' && c <= L'ﾝ') ||
          (c >= L'一' && c <= L'龯') ||
          (c >= L'㐀' && c <= L'䶵'));
}

bool Syntax::is_whitespace(const wchar_t c) noexcept
{
  return c == L'　' || c == L' ';
}

bool Syntax::is_keyword(const std::wstring_view key) noexcept
{
  return keyword_map.find(key) != keyword_map.end();
}

bool Syntax::is_operator(const std::wstring_view key) noexcept
{
  return operator_map.find(key) != operator_map.end();
}

const StringifiedReservedToken& Syntax::get_keyword(const std::wstring_view key)
{
  assert(is_keyword(key));
  return SyntaxConstants::keywords[keyword_map.find(key)->second];
}

const StringifiedReservedToken& Syntax::get_operator(const std::wstring_view key)
{
  assert(is_operator(key));
  return SyntaxConstants::operators[operator_map.find(key)->second];
}
