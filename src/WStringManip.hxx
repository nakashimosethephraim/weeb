/* 
 * File:   "WStringManip.hxx"
 * Author: Seth Nakashimo
 * Date:   2022/02/06
 *
 * A header file defining functionality useful for manipulating
 * std::wstring.
 */

#ifndef WSTRINGMANIP_HXX
#define WSTRINGMANIP_HXX

#include <string>

namespace WStringManip
{
  void trim(std::wstring& s);
  std::wstring convert(const std::string& source);
}

#endif /* WSTRINGMANIP_HXX */