/*
 * File:   SyntaxToken.hxx
 * Author: Seth Nakashimo
 * Date:   2023/02/03
 */

#ifndef SYNTAXTOKEN_HXX
#define SYNTAXTOKEN_HXX

#include <variant>
#include <string>
#include <string_view>

#include "Syntax.hxx"

class SyntaxToken
{
public:
  enum class SyntaxTokenDataType
  {
    Unknown,
    ReservedToken,
    Identifier,
    Number,
    String,
    Whitespace,
    Indentation,
    Newline,
    BadToken,
    Eof,
    Count
  };

  class SyntaxTokenData
  {
  protected:
    SyntaxTokenDataType data_type;

    SyntaxTokenData(SyntaxTokenDataType data_type) noexcept : data_type(data_type) {}
    virtual std::wostream& print(std::wostream& stream) const;

  public:
    SyntaxTokenData() noexcept = delete;
    const SyntaxTokenDataType get_interface() const { return data_type; }
    friend std::wostream& operator<<(std::wostream& stream, const SyntaxTokenData& token);
  };

  SyntaxToken(std::unique_ptr<SyntaxTokenData> data, size_t start_position, size_t end_position) :
    data_{ std::move(data) }, start_position(start_position),
    end_position(end_position) {}

  [[nodiscard]]
  const SyntaxTokenData& value() const noexcept
  {
    return *data_;
  }

  friend std::wostream& operator<<(std::wostream& stream, const SyntaxToken& token);

private:
  std::unique_ptr<SyntaxTokenData> data_;
  size_t start_position, end_position;
};

class ReservedTokenSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  const StringifiedReservedToken& reserved_token;
  std::wostream& print(std::wostream& stream) const override;

public:
  ReservedTokenSyntaxTokenData(const StringifiedReservedToken& reserved_token) :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::ReservedToken),
    reserved_token(reserved_token) {}
};

class IdentifierSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  const std::wstring name;
  std::wostream& print(std::wostream& stream) const override;

public:
  IdentifierSyntaxTokenData(const std::wstring_view name) :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Identifier),
    name(name) {}
};

class NumberSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  const std::uint32_t number;
  std::wostream& print(std::wostream& stream) const override;

public:
  NumberSyntaxTokenData(const std::uint32_t number) :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Number),
    number(number) {}
};

class StringSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  const std::wstring value;
  std::wostream& print(std::wostream& stream) const override;

public:
  StringSyntaxTokenData(const std::wstring_view value) :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::String),
    value(value) {}
};

class WhitespaceSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  std::wostream& print(std::wostream& stream) const override;

public:
  WhitespaceSyntaxTokenData() :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Whitespace) {}
};

class IndentationSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  std::wostream& print(std::wostream& stream) const override;

public:
  IndentationSyntaxTokenData() :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Indentation) {}
};

class NewlineSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  std::wostream& print(std::wostream& stream) const override;

public:
  NewlineSyntaxTokenData() :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Newline) {}
};

class BadTokenSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  const std::wstring value;
  std::wostream& print(std::wostream& stream) const override;

public:
  BadTokenSyntaxTokenData(const std::wstring_view value) :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::BadToken),
    value(value) {}
};

class EofSyntaxTokenData : public SyntaxToken::SyntaxTokenData
{
private:
  std::wostream& print(std::wostream& stream) const override;

public:
  EofSyntaxTokenData() :
    SyntaxToken::SyntaxTokenData(SyntaxToken::SyntaxTokenDataType::Eof) {}
};

#endif /* SYNTAXTOKEN_HXX */