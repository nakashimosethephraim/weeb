/* 
 * File:   "JapaneseStringUtilities.cxx"
 * Author: Seth Nakashimo
 * Date:   2022/02/06
 */

#include "JapaneseStringUtilities.hxx"

#include "Syntax.hxx"

const std::uint32_t JapaneseStringUtilities::wston(const std::wstring_view num_string)
{
  assert(!num_string.empty());

  std::uint32_t value = 0, place = 1;
  for (std::int32_t i = num_string.length() - 1; i >= 0; --i)
  {
    assert(Syntax::is_digit(num_string[i]));
    switch (num_string[i])
    {
      case L'０':
        break;
      case L'１':
        value += place;
        break;
      case L'２':
        value += place * 2;
        break;
      case L'３':
        value += place * 3;
        break;
      case L'４':
        value += place * 4;
        break;
      case L'５':
        value += place * 5;
        break;
      case L'６':
        value += place * 6;
        break;
      case L'７':
        value += place * 7;
        break;
      case L'８':
        value += place * 8;
        break;
      case L'９':
        value += place * 9;
        break;
    }

    place *= 10;
  }

  return value;
}