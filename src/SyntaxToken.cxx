/*
 * File:   SyntaxToken.cxx
 * Author: Seth Nakashimo
 * Date:   2023/02/07
 */

#include "SyntaxToken.hxx"

[[nodiscard]]
std::wostream& operator<<(std::wostream& stream, const SyntaxToken& token)
{
  stream << *token.data_;
  return stream;
}

[[nodiscard]]
std::wostream& operator<<(std::wostream& stream, const SyntaxToken::SyntaxTokenData& token)
{
  return token.print(stream);
}

std::wostream& SyntaxToken::SyntaxTokenData::print(std::wostream& stream) const
{
  return stream;
}

std::wostream& ReservedTokenSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: ReservedToken" << std::endl
         << reserved_token;
  return stream;
}

std::wostream& IdentifierSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Identifier" << std::endl
         << L"Name: " << name;
  return stream;
}

std::wostream& NumberSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Number" << std::endl
         << L"Number: " << number;
  return stream;
}

std::wostream& StringSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: String" << std::endl
         << L"Value: \"" << value << "\"";
  return stream;
}

std::wostream& WhitespaceSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Whitespace";
  return stream;
}

std::wostream& IndentationSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Indentation";
  return stream;
}

std::wostream& NewlineSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Newline";
  return stream;
}

std::wostream& BadTokenSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: BadToken" << std::endl
         << L"Value: \"" << value << "\"";
  return stream;
}

std::wostream& EofSyntaxTokenData::print(std::wostream& stream) const
{
  stream << L"Type: Eof";
  return stream;
}