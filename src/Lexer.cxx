/*
 * File:   Lexer.cxx
 * Author: Seth Nakashimo
 * Date:   2023/02/06
 */

#include "Lexer.hxx"

#include "JapaneseStringUtilities.hxx"

const wchar_t Lexer::current() const
{
  if (position >= text.length()) return L'\0';

  return text[position];
}

std::unique_ptr<SyntaxToken> Lexer::next_token()
{
  if (text.empty())
  {
    return std::make_unique<SyntaxToken>(std::make_unique<EofSyntaxTokenData>(), position, position);
  }

  if (position == text.length())
  {
    return std::make_unique<SyntaxToken>(std::make_unique<NewlineSyntaxTokenData>(), position, position);
  }

  if (Syntax::is_string_begin(current()))
  {
    auto start = position;

    do { next(); }
    while (!Syntax::is_string_end(current()) && position < text.length());

    if (position < text.length()) next();

    if (Syntax::is_string_end(text[position - 1]))
    {
      auto length = position - start - 2;
      if (length <= 0)
      {
        return std::make_unique<SyntaxToken>(std::make_unique<StringSyntaxTokenData>(L""), start, position - 1);
      }
      else
      {
        const auto& new_text = text.substr(start + 1, length);
        return std::make_unique<SyntaxToken>(std::make_unique<StringSyntaxTokenData>(new_text), start, position - 1);
      }
    }
    else
    {
      auto length = position - start;
      const auto& new_text = text.substr(start, length);
      return std::make_unique<SyntaxToken>(std::make_unique<BadTokenSyntaxTokenData>(new_text), start, position - 1);
    }
  }

  if (Syntax::is_digit(current()))
  {
    auto start = position;
    next();

    while (Syntax::is_digit(current())) next();

    auto length = position - start;
    const auto& new_text = text.substr(start, length);

    return std::make_unique<SyntaxToken>(std::make_unique<NumberSyntaxTokenData>(JapaneseStringUtilities::wston(new_text)), start, position - 1);
  }

  if (Syntax::is_whitespace(current()))
  {
    auto start = position;
    next();

    if (position == 0)
    {
      size_t whitespace_count = 1;

      while (Syntax::is_whitespace(current()) && whitespace_count < 4)
      {
        ++whitespace_count;
        next();
      }

      auto length = position - start;
      const auto& new_text = text.substr(start, length);

      if (whitespace_count == 4)
      {
        return std::make_unique<SyntaxToken>(std::make_unique<IndentationSyntaxTokenData>(), start, position - 1);
      }
      else return std::make_unique<SyntaxToken>(std::make_unique<BadTokenSyntaxTokenData>(new_text), start, position - 1);
    }
    else
    {
      while (Syntax::is_whitespace(current())) next();

      auto length = position - start;
      const auto& new_text = text.substr(start, length);

      return std::make_unique<SyntaxToken>(std::make_unique<WhitespaceSyntaxTokenData>(), start, position - 1);
    }
  }

  if (Syntax::is_valid_operator(current()))
  {
    auto start = position;
    next();

    while (Syntax::is_valid_operator(current())) next();

    auto length = position - start;
    const auto& new_text = text.substr(start, length);

    if (Syntax::is_operator(new_text))
    {
      return std::make_unique<SyntaxToken>(std::make_unique<ReservedTokenSyntaxTokenData>(Syntax::get_operator(new_text)), start, position - 1);
    }
    else return std::make_unique<SyntaxToken>(std::make_unique<BadTokenSyntaxTokenData>(new_text), start, position - 1);
  }

  if (Syntax::is_valid_identifier(current()))
  {
    auto start = position;
    next();

    while (Syntax::is_valid_identifier(current())) next();

    auto length = position - start;
    const auto& new_text = text.substr(start, length);

    if (!Syntax::is_keyword(new_text))
    {
      return std::make_unique<SyntaxToken>(std::make_unique<IdentifierSyntaxTokenData>(new_text), start, position - 1);
    }
    else
    {
      return std::make_unique<SyntaxToken>(std::make_unique<ReservedTokenSyntaxTokenData>(Syntax::get_keyword(new_text)), start, position - 1);
    }
  }

  auto start = position;
  next();
  auto length = position - start;
  const auto& new_text = text.substr(start, length);

  return std::make_unique<SyntaxToken>(std::make_unique<BadTokenSyntaxTokenData>(new_text), start, position - 1);
}