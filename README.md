# Weeb++

## Description
This is a compiler for a language wherein all syntactic components must be written in Japanese.

## Installation
TODO

## Usage
TODO

## Support
Issues should be filed against this repository.

## Roadmap
I hope to have the compiler working to a basic extent within 2023.

## Contributing
For the time being, I am not open to contributions, as this is a personal project for myself. Once "complete", I would be open to anyone contributing.

## License
This project is licensed under the MIT License.

## Project status
I am actively trying to develop the compiler.
